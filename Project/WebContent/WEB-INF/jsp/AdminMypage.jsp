<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ管理</title>
<link href="css/History.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<form action="AdminMypage" method="post">
			<div class="loginID">
				<p>ログインID:</p>
				<input type="text" name="login-id" id="login-id">
			</div>

			<div class="user_name">
				<p>名前:</p>
				<input type="text" name="user-name" id="user-name">
			</div>

			<div class="BirthDay">
				<label for="continent">生年月日</label>
				<div class="row">
					<div class="col-sm-2">
						<input type="date" name="date" id="date"
							class="form-control" size="30" />
					</div>
				</div>
			</div>

			<div class="botton">
				<input type="submit" class="Button" value="検索">
			</div>
		</form>


	<div class="contents">
		<table class="table" border="1">
			<thead>
				<tr>
					<th>ログインID</th>
					<th>名前</th>
					<th>生年月日</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="user" items="${userList}">
					<tr>
						<td>${user.loginId}</td>
						<td>${user.name}</td>
						<td>${user.birthDate}</td>
						<td>
							<div class="btn-group" role="group" aria-label="Basic example">

								 <a href="UserUpdata?id=${user.id}">
									<button type="button" class="btn2">更新</button>
								</a>

								<a href="UserDelete?id=${user.id}">
									<button type="button" class="btn3">削除</button>
								</a>
							</div>
						</td>
					</tr>

				</c:forEach>
			</tbody>
		</table>
	</div>
	<a href="TopPage">戻る</a>
</body>
</html>