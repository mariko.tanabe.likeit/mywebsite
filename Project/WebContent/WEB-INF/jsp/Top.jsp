<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>TOP</title>
    <link href="css/Top.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <header>
        <h2 >TOP</h2>
        <div class="container">
            <a class="head" href="News">最新情報</a>
            <a class="head" href="GoodsList?id=${user.id}">グッズ</a>
             <c:if test="${userInfo.name.equals('管理者')}"><a class="head" href="AdminMypage">ユーザ管理用ページ</a></c:if>
             <a class="head" href="MyPage?id=${userInfo.id}">マイページ</a>
            <a class="head2" href="Logout">ログアウト</a>
            <c:if test="${userInfo.name.equals('管理者')}"><a class="head2" href="ListOfRequests">問い合わせ一覧</a></c:if>
        </div>

    </header>

    <div class="container">
        <h2>Profile</h2>
        <p>
            ABC DEF<br>
        </p>
        <p>
            誕生日: 1990.8.1<br>
            身長: 176cm<br>
            血液型:AB型
            趣味: サウナ巡り<br>
            特技: ダンス、水泳<br>
        </p>
        <h2>work</h2>

            ~舞台~<br>
            <ul>
                <li>ミュージカル『鬼』 - 藤 役</li>
                <li>A The LIVE - 主演・純 役</li>
                <li>オン！ - 主演・佐々木 役</li>
                <li>男水 - 平光希 役</li>
            </ul>

            ~TV~<br>
            <ul>
                <li>ドラマ</li>
                <li>ラーメンCM</li>
                <li>お天気</li>
                <li>映画</li>
            </ul>



    </div>


</body>
</html>