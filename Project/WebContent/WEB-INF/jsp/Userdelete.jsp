<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ削除</title>
    <link rel="stylesheet" href="css/Delete.css">
</head>
<body>
    <header>
        <h1>ユーザ削除確認</h1>
    </header>
	<form action="UserDelete" method="post">
		<input type="hidden" value="${user.id}" name="id">
    <div class="contents">
        <p>${user.name}</p>
        <p>のアカウントを本当に削除してよろしいでしょうか。</p>
    </div>

    <div class="botton">
        <a href="Login"><input class="btn1" type="submit" class="okButton" value="OK"></a>
       <c:if test="!(${userInfo.name.equals('管理者')})"> <a href="Mypage?id=${user.id}" ><input class="btn1" type="button" value="キャンセル"></a></c:if>
       <c:if test="${userInfo.name.equals('管理者')}"><a class="head" href="AdminMypage">戻る</a></c:if>
     </div>
	</form>
    <img src="https://1.bp.blogspot.com/-oapWEXWcCNY/WD_cZAZbqSI/AAAAAAABAE4/2h45soF1qzQ1kco9MgojjQ9mcja9XEwKQCLcB/s800/tobotobo_aruku_woman.png"
         height="50%" width="50%">
</body>
</html>