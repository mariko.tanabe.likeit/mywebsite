<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>お問い合わせ</title>
    <link href="css/Contact.css" rel="stylesheet" type="text/css" />
  </head>
  <body>



      <div class="contact-form">

        <h3 class="section-title">お問い合わせフォーム</h3>
        <form action="Contacts" method="post">
        	<input type="hidden" value="${user.id}" name="id">
          <p>メールアドレス（必須）</p>
          <input type="text" name="mailaddress" class="content">
          <p>お問い合わせ内容（必須）</p>
            <textarea class="content" cols="40" rows="4" name="detail"></textarea>


        <input type="submit" value="送信" class="button">
        </form>



      </div>

  </body>
</html>