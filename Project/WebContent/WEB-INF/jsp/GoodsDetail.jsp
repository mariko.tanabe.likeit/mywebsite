<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>詳細</title>
    <link href="css/GoodsDetail.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <header>
      <h2>グッズ詳細</h2>
      </header>


      <div class="main-contents">
          <h3>${item.itemName}</h3>
          <img src="${item.image}"
               height="40%" width="40%">
          <p>${item.detail} </p>
          <p>${item.itemPrice}円(税込み)</p>



        <div >
            <a href="Cart?id=${item.id}"><input type="submit" value="購入" class="btn1"></a>
            <c:if test="${userInfo.name.equals('管理者')}"><a href="GoodsDelete?id=${item.id}"><input type="submit" value="削除" class="btn2"></a>
            <a href="GoodsUpdata?id=${item.id}"><input type="submit" value="更新" class="btn3"></a></c:if>
        </div>


        <a class="head" href="GoodsList">戻る</a>
      </div>

  </body>
</html>