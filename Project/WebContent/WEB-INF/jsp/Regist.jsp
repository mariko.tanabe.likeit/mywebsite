<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ新規登録</title>
    <link href="css/Regist.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <header>
        <h1>ユーザ新規登録</h1>
    </header>
	<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>
	<form action="Regist" method="post">
        <div class="loginId">
            <p>ログインID</p>
            <input type="text"  name="loginId" id="loginId"  placeholder="ID">

        </div>

        <div class="password1">
            <p>パスワード</p>
            <input type="text" name="password1" id="password1" placeholder="パスワード">
        </div>

        <div class="password2">
            <p>パスワード(確認)</p>
            <input type="text" name="password2" id="password2" placeholder="パスワード(確認)">
        </div>

        <div class="name">
            <p>氏名</p>
            <input type="text" name="name" id="name" placeholder="氏名">
        </div>

        <div class="address">
            <p>住所</p>
            <input type="text" name="address" id="address" placeholder="住所">
        </div>

        <div class="userName">
            <p>ユーザ名</p>
            <input type="text" name="userName" id="userName" placeholder="ユーザ名">
        </div>

        <div class="phoneNumber">
            <p>電話番号</p>
            <input type="text" name="phoneNumber" id="phoneNumber" placeholder="電話番号">
        </div>

         <div class="birth">
            <p>生年月日</p>
            <input type="date" name="birth" id="birth" placeholder="生年月日">
        </div>
    <div>
        <input type="submit" value="登録" class="button" name="action">
    </div>
    </form>
        <a href="Login.html">戻る</a>
</body>
</html>