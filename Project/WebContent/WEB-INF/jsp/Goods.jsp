<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
    <meta charset="UTF-8">
    <title>TOP</title>
    <link href="css/Goods.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <header>
        <h2>グッズ</h2>
        <img  class="image" src="https://1.bp.blogspot.com/-1j4k7xTUMiE/Xr33DNA8-oI/AAAAAAABY6g/cZfSh4LanWIqC51gZYokNvdZIFLEw8JbACNcBGAsYHQ/s1600/nikkouyoku_woman.png">
        <div class="container">
            <a class="head" href="TopPage?id=${userInfo.id}">TOP</a>
            <a class="head" href="News.jsp">最新情報</a>
            <a class="head" href="MyPage?id=${userInfo.id}">マイページ</a>
            <a class="head2" href="Logout">ログアウト</a>
            <c:if test="${userInfo.name.equals('管理者')}"><a class="head2" href="GoodsAdd">グッズ追加</a></c:if>
        </div>

    </header>

	<div class="container row">
		<c:forEach var="item" items="${itemList}">
		<div class="col s12 m3">
			<a href="ItemDetail?id=${item.id}"><img src="${item.image}"
				width="150px" height="150px" ></a>
			<p>${item.itemName} ${item.itemPrice}円</p>
			<a href="Cart?id=${item.id}">
				<button type="submit" class="btn1">購入</button>
			</a>
		</div>
		</c:forEach>
	</div>
</body>
</html>