<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>購入完了</title>
    <link href="css/GoodsDetail.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <header>
        <h1>購入完了</h1>
    </header>


    <p>お買い上げいただき、ありがとうございます。</p>

    <a href="GoodsList">グッズ一覧へ</a>
</body>

</html>