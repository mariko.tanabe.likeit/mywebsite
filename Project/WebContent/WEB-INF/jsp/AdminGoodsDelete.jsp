<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>グッズ削除</title>
    <link rel="stylesheet" href="css/Delete.css">
</head>
<body>
    <header>
        <h1>グッズ削除</h1>
    </header>
	<form action="GoodsDelete" method="post">
		<input type="hidden" value="${item.id}" name="id">
    <div class="contents">
        <p>${item.itemName}</p>
        <img src="${item.image}"
               height="40%" width="40%">
        <p>を本当に削除してよろしいでしょうか。</p>
    </div>

    <div class="botton">
        <input class="btn1" type="submit" class="okButton" value="OK">
        <a href="ItemDetail?id=${item.id}" ><input class="btn1" type="button" value="キャンセル"></a>
     </div>
	</form>
</body>
</html>