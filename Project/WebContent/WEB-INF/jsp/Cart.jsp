<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<meta charset="UTF-8">
<title>買い物かご</title>
<link href="css/GoodsDetail.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<header>
		<h1>買い物かご</h1>
		${cartActionMessage}
	</header>


	<div class="row">
		<c:forEach var="item" items="${cart}" varStatus="status">
			<img src="${item.image}" width="150px" height="150px">
			<p>${item.itemName}</p>
		</c:forEach>
		<a href="Purchase"><input type="submit" value="進む" class="btn1"></a>
		<a href="GoodsList"><input type="submit" value="戻る" class="btn2"></a>
	</div>
</body>

</html>