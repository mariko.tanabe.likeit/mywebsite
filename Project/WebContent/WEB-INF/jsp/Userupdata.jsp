<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ情報更新</title>
    <link rel="stylesheet" href="css/Regist.css">
</head>
<body>
    <header>
        <h1>ユーザ情報更新</h1>
    </header>
	<c:if  test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>
	<form  action="UserUpdata" method="post">

		<input type="hidden" value="${user.id}" name="id">

        <div class="password1">
            <p>パスワード</p>
            <input type="password" name="password1" id="password1" placeholder="パスワード">
        </div>

        <div class="password2">
            <p>パスワード(確認)</p>
            <input type="password" name="password2" id="password2" placeholder="パスワード(確認)">
        </div>

        <div class="address">
            <p>住所</p>
            <input type="text" name="address" id="address" value="${user.address }">
        </div>

        <div class="userName">
            <p>ユーザ名</p>
            <input type="text" name="userName" id="userName" value="${user.userName }">
        </div>

        <div class="phoneNumber">
            <p>電話番号</p>
            <input type="text" name="phoneNumber" id="phoneNumber" value="${user.phoneNumber }">
        </div>

    <div>
        <input type="submit" value="更新" class="button">
    </div>
    </form>
       <c:if test="!(${userInfo.name.equals('管理者')})"><a href="MyPage?id=${user.id}">戻る</a></c:if>
       <c:if test="${userInfo.name.equals('管理者')}"><a class="head" href="AdminMypage">戻る</a></c:if>
    </body>
</html>