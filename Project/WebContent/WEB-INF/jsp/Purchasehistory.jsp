<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入履歴</title>
<link href="css/History.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<header>
		<h2>購入履歴</h2>
	</header>

	<div class="contents">
		<table class="table" border="1">
			<thead>
				<tr>
					<th>購入日</th>
					<th>合計金額</th>
					<th>送料</th>
				</tr>
			</thead>
			<tbody>

				<c:forEach var="bd" items="${buylist}">
					<tr>
						<td>${bd.formatDate}</td>
						<td>${bd.totalPrice}円</td>
						<td>400円</td>
					</tr>
				</c:forEach>

			</tbody>

		</table>

		<a class="head" href="MyPage?id=${userInfo.id}">戻る</a>




	</div>


</body>
</html>