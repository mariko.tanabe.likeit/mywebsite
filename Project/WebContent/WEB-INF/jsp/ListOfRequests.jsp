<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>一覧</title>
<link href="css/Requests.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<header>
		<h2>問い合わせ一覧</h2>
	</header>


	<div class="main-contents">
		<c:forEach var="list" items="${List}">
			<p class="mailadress">${list.mailaddress}</p>
			<div class="contents">
				<p>${list.contents}</p>
			</div>



			<form action="ListOfRequests" method="post">
				<input type="hidden" value="${list.id}" name="id"> <input
					type="submit" value="削除" class="btn1">
			</form>
		</c:forEach>

	</div>

</body>
</html>