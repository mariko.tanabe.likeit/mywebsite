<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ情報更新</title>
    <link rel="stylesheet" href="css/Contact.css">
</head>
<body>
    <header>
        <h1>グッズ情報の更新</h1>
        <h3>変更箇所を編集すること。</h3>
    </header>

         <div class="main-contents">
			<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

          <form action="GoodsUpdata" method="post">
			<input type="hidden" value="${item.id}" name="id">
              	<p>グッズ名</p>
              	<input type="text" name="itemName" value="${item.itemName}" class="content">

                <p>グッズの説明</p>
                    <textarea class="content" cols="40" rows="4" name="detail" >${item.detail}</textarea>

                 <p>値段</p>
              	<input type="text" name="price" value="${item.itemPrice}" class="content">

              <p>
                  <input type="submit" value="更新" class="button">
              </p>
          </form>







      </div>
        <a href="GoodsList?id=${userInfo.id}">戻る</a>
    </body>
</html>