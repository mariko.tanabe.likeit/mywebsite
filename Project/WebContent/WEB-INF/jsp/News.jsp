<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>TOP</title>
    <link href="css/News.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <header>
        <h2 >最新情報</h2>
        <div class="container">
            <a class="head" href="TopPage?id=${user.id}">TOP</a>
            <a class="head" href="GoodsList">グッズ</a>
            <c:if test="${userInfo.name.equals('管理者')}"><a class="head" href="AdminMypage">ユーザ管理用ページ</a></c:if>
            <a class="head" href="MyPage?id=${userInfo.id}">マイページ</a>
            <a class="head2" href="Logout">ログアウト</a>
        </div>

    </header>

    <div class="container">
        <h2>活動休止のお知らせ</h2>
            <p>諸般の事情により、2020年12月末まで活動を休止いたします。</p>
        <img src="https://1.bp.blogspot.com/-OakQR6nTlbs/XexqgY_oNwI/AAAAAAABWgc/H4N5F3gevTUEblXhaHxcksEvzSjm_vf1ACNcBGAsYHQ/s1600/gassyuku_shokuji_boys.png" height="20%"
             width="20%">
    </div>


</body>
</html>