<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入</title>
<link href="css/History.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<header>
		<h2>購入</h2>
	</header>

	<div class="contents">
		<table class="table" border="1">
			<thead>
				<tr>
					<th>商品名</th>
					<th>金額</th>
					<th>小計</th>

				</tr>
			</thead>
			<tbody>
				<c:forEach var="cartInItem" items="${cart}">
				<tr>
					<td>${cartInItem.itemName}</td>
					<td>${cartInItem.formatPrice}円</td>
					<td>${cartInItem.formatPrice}円</td>
					</tr>
				</c:forEach>
				<tr>
					<td>送料</td>
					<td></td>
					<td>400円</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td>合計</td>
					<td></td>
					<td>${bdb.formatTotalPrice}円</td>
				</tr>
			</tfoot>

		</table>

		<form action="PurchaseComplete" method="post"><button type="submit"
				class="btn">購入</button></form> <a class="head" href="GoodsList">戻る</a>




	</div>


</body>
</html>