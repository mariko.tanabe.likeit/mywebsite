<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>TOP</title>
    <link href="css/Mypage.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <header>
        <h2 >マイページ</h2>
        <div class="container">
            <a class="head" href="TopPage?id=${user.id}">TOP</a>
            <a class="head" href="News">最新情報</a>
            <a class="head" href="GoodsList?id=${user.id}">グッズ</a>
            <a class="head2" href="Logout">ログアウト</a>
        </div>

    </header>

    <div class="contents">
        <p>ファンクラブナンバー:${user.id}</p>
        <p>${user.userName}さんは、${user.createDate}に入会しました</p>
        <p>設定の変更は<a class="Updata" href="UserUpdata?id=${user.id}">こちら</a></p>
        <p>購入履歴は<a class="history" href="PurchaseHistory?id=${user.id}">こちら</a></p>
        <p>退会は<a class="userDelete" href="UserDelete?id=${user.id}">こちら</a></p>
        <p>お問い合わせは<a class="contact" href="Contacts?id=${user.id}">こちら</a></p>




    </div>


</body>
</html>