<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>グッズ追加</title>
    <link href="css/Contact.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <header>
      <h2>グッズの追加</h2>
      </header>


      <div class="main-contents">

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

          <form action="GoodsAdd" method="post" >
              <p>グッズの画像</p>
                  <input type="text" name="datafile" class="content">

              	<p>グッズ名</p>
              		<input type="text" name="itemName" class="content">
                <p>グッズの説明</p>
                    <textarea class="content" cols="40" rows="4" name="detail"></textarea>

              <p>値段</p>
              	<input type="text" name="price" class="content">


                  <input type="submit" value="追加" class="button">

          </form>







      </div>

  </body>
</html>