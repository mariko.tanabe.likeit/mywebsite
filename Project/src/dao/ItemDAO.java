package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ItemDataBeans;
public class ItemDAO {
	public void itemRegist(String image,String name,String detail,String price) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			PreparedStatement pstmt = null;
			String insertSQL="INSERT INTO t_item (item_name,price,detail,image) VALUES(?,?,?,?)";
			pstmt=conn.prepareStatement(insertSQL);

			pstmt.setString(1, name);
			pstmt.setString(2, price);
			pstmt.setString(3, detail);
			pstmt.setString(4,image);


			pstmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public static ArrayList<ItemDataBeans> getItemAllData() throws SQLException{
		Connection con=null;
		PreparedStatement st=null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_item");

			ResultSet rs=st.executeQuery();

			ArrayList<ItemDataBeans> itemList=new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setItemName(rs.getString("item_name"));
				item.setItemPrice(rs.getInt("price"));
				item.setDetail(rs.getString("detail"));
				item.setImage(rs.getString("image"));
				itemList.add(item);
			}
			System.out.println("getAllItem completed");
			return itemList;

		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public ItemDataBeans itemDetail(String id) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			String sql="SELECT * FROM t_item WHERE id=?";
			PreparedStatement pStmt=conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs=pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

				int ID=rs.getInt("id");
				String ItemName=rs.getString("item_name");
				String Detail=rs.getString("detail");
				int ItemPrice=rs.getInt("price");
				String Image=rs.getString("image");
				ItemDataBeans itemData=new ItemDataBeans(ID,ItemName,ItemPrice,Detail,Image);


				return itemData;

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void GoodsUpdata(String itemName,String detail,String price,String id) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			PreparedStatement pstmt = null;
			String insertSQL="UPDATE t_item SET item_name=?, detail=?, price=? WHERE id=?";
			pstmt=conn.prepareStatement(insertSQL);


			pstmt.setString(1, itemName);
			pstmt.setString(2, detail);
			pstmt.setString(3, price);
			pstmt.setString(4,id);

			pstmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public void itemDelete(String id) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			PreparedStatement pstmt = null;
			String insertSQL="DELETE FROM t_item WHERE id=?";
			pstmt=conn.prepareStatement(insertSQL);

			pstmt.setString(1,id);

			pstmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public static ItemDataBeans getItemByItemID(int itemId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_item WHERE id = ?");
			st.setInt(1, itemId);

			ResultSet rs = st.executeQuery();

			ItemDataBeans item = new ItemDataBeans();
			if (rs.next()) {
				item.setId(rs.getInt("id"));
				item.setItemName(rs.getString("item_name"));
				item.setDetail(rs.getString("detail"));
				item.setItemPrice(rs.getInt("price"));
				item.setImage(rs.getString("image"));
			}

			System.out.println("searching item by itemID has been completed");

			return item;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static int getTotalItemPrice(ArrayList<ItemDataBeans> items) {
		int total = 0;
		for (ItemDataBeans item : items) {
			total += item.getItemPrice();
		}
		total+=400;
		return total;
	}

	public List<ItemDataBeans> getItemDetail(String buyId) throws SQLException {
		List<ItemDataBeans> itemData=new ArrayList<ItemDataBeans>();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT item_id, price FROM buy_data INNER JOIN buy_detail ON buy_data.id=buy_detail.buy_id INNER JOIN t_item ON buy_detail.item_id=t_item.id WHERE buy_id = ?");
			st.setString(1, buyId);

			ResultSet rs = st.executeQuery();


			while (rs.next()) {
				int id=rs.getInt("item_id");
				int priCe=rs.getInt("price");
				ItemDataBeans itemDetail=new ItemDataBeans(id,priCe);
				itemData.add(itemDetail);
			}

			System.out.println("アイテム情報");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
		return itemData;
	}
}
