package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserDataBeans;


public class UserDAO {
	public String Code(String password){
		String source = password;
		String result="";
		Charset charset = StandardCharsets.UTF_8;

		String algorithm = "MD5";


		byte[] bytes;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));

			result = DatatypeConverter.printHexBinary(bytes);

			System.out.println(result);

		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return result;

	}

	public UserDataBeans findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql="SELECT * FROM t_user WHERE login_id =? and password =?";
			PreparedStatement pStmt=conn.prepareStatement(sql);

			pStmt.setString(1,loginId);
			pStmt.setString(2, password);
			ResultSet rs=pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int IdData=rs.getInt("id");
			String nameData=rs.getString("name");
			return new UserDataBeans(IdData,nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public UserDataBeans findById(String loginId) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			String sql="SELECT * FROM t_user WHERE login_id = ?";
			PreparedStatement pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, loginId);
			ResultSet rs=pstmt.executeQuery(sql);

			if(!rs.next()) {
				return null;
			}

			String loginIdData=rs.getString("login_id");
			return new UserDataBeans(loginIdData);


		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;

	}

	public void userRegist(String loginId,String password,String name,String address,String userName, String phoneNumber,String birthDate) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			PreparedStatement pstmt = null;
			String insertSQL="INSERT INTO t_user (login_id,name,birth_date,password,address,user_name,phonenumber,create_date) VALUES(?,?,?,?,?,?,?,now())";
			pstmt=conn.prepareStatement(insertSQL);

			pstmt.setString(1, loginId);
			pstmt.setString(2, name);
			pstmt.setString(3, birthDate);
			pstmt.setString(4,password);
			pstmt.setString(5,address);
			pstmt.setString(6,userName);
			pstmt.setString(7,phoneNumber);



			pstmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public List<UserDataBeans> findAll(){
		Connection conn=null;
		List<UserDataBeans> userList=new ArrayList<UserDataBeans>();
		try {
			conn=DBManager.getConnection();
			String sql="SELECT*FROM t_user WHERE name != '管理者'";

			Statement stmt=conn.createStatement();
			ResultSet rs=stmt.executeQuery(sql);

			while(rs.next()) {
				int id=rs.getInt("id");
				String loginId=rs.getString("login_id");
				String password=rs.getString("password");
				String name=rs.getString("name");
				String address=rs.getString("address");
				String userName=rs.getString("user_name");
				int phoneNumber=rs.getInt("phonenumber");
				Date birthDate=rs.getDate("birth_date");
				String createDate=rs.getString("create_date");

				UserDataBeans user=new UserDataBeans(id,loginId,password,name,address,userName,phoneNumber,birthDate,createDate);

				userList.add(user);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public UserDataBeans userDetail(String id) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			String sql="SELECT * FROM t_user WHERE id=?";
			PreparedStatement pStmt=conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs=pStmt.executeQuery();

			if(rs.next()) {
				int ID=rs.getInt("id");
				String loginId=rs.getString("login_id");
				String password=rs.getString("password");
				String name=rs.getString("name");
				String address=rs.getString("address");
				String userName=rs.getString("user_name");
				int phoneNumber=rs.getInt("phonenumber");
				Date birthDate=rs.getDate("birth_date");
				String createDate=rs.getString("create_date");

				UserDataBeans user=new UserDataBeans(ID,loginId,password,name,address,userName,phoneNumber,birthDate,createDate);

				return user;
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	public void userDelete(String id) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			PreparedStatement pstmt = null;
			String insertSQL="DELETE FROM t_user WHERE id=?";
			pstmt=conn.prepareStatement(insertSQL);

			pstmt.setString(1,id);

			pstmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public void userUpdata(String password,String address,String name,String phoneNumber,String id) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			PreparedStatement pstmt = null;
			String insertSQL="UPDATE t_user SET address=?,user_name=?,phonenumber=?,password=? WHERE id=?";
			pstmt=conn.prepareStatement(insertSQL);


			pstmt.setString(1, address);
			pstmt.setString(2, name);
			pstmt.setString(3, phoneNumber);
			pstmt.setString(4,password);
			pstmt.setString(5, id);

			pstmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public void userUpdata2(String address,String name,String phoneNumber,String id) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			PreparedStatement pstmt = null;
			String insertSQL="UPDATE t_user SET address=?, user_name=?, phonenumber=? WHERE id=?";
			pstmt=conn.prepareStatement(insertSQL);


			pstmt.setString(1, address);
			pstmt.setString(2, name);
			pstmt.setString(3, phoneNumber);
			pstmt.setString(4,id);

			pstmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public List<UserDataBeans> serch(String loginId, String name, String birth) {
		List<UserDataBeans> serchList=new ArrayList<UserDataBeans>();
		Connection conn = null;
		conn = DBManager.getConnection();
		try {
			String sql="SELECT * FROM t_user WHERE login_id !='admin'";

			if(!loginId.equals("")) {
				sql+="AND login_id ='" + loginId + "'";
			}
			if(!name.equals("")) {
				sql+="AND name LIKE '%" + name + "%'";
			}
			if(!birth.equals("")) {
				sql+="AND birth_date = "+birth+" ";
			}


			PreparedStatement pStmt=conn.prepareStatement(sql);
			System.out.println(sql);
			ResultSet rs=pStmt.executeQuery();

			while(rs.next()) {
				int ID=rs.getInt("id");
				String loginID=rs.getString("login_id");
				String naMe=rs.getString("name");
				Date birthDate=rs.getDate("birth_date");
				UserDataBeans user=new UserDataBeans(ID,loginID,naMe,birthDate);

				serchList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return serchList;
	}
}
