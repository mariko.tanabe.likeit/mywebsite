package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.ContactDataBeans;
public class ContactsDAO {

	public void contactRegist(String id,String address,String detail) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			PreparedStatement pstmt = null;
			String insertSQL="INSERT INTO contact_t (user_id,mailaddress,contents) VALUES(?,?,?)";
			pstmt=conn.prepareStatement(insertSQL);

			pstmt.setString(1,id);
			pstmt.setString(2, address);
			pstmt.setString(3, detail);


			pstmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public static ArrayList<ContactDataBeans> getAllData() throws SQLException{
		Connection con=null;
		PreparedStatement st=null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM contact_t");

			ResultSet rs=st.executeQuery();

			ArrayList<ContactDataBeans> List=new ArrayList<ContactDataBeans>();

			while (rs.next()) {
				ContactDataBeans detail = new ContactDataBeans();
				detail.setId(rs.getInt("id"));
				detail.setUserId(rs.getInt("user_id"));
				detail.setMailaddress(rs.getString("mailaddress"));
				detail.setContents(rs.getString("contents"));

				List.add(detail);
			}
			System.out.println("getAllContents completed");
			return List;

		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public void contactDelete(String id) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();
			PreparedStatement pstmt = null;
			String insertSQL="DELETE FROM contact_t WHERE id=?";
			pstmt=conn.prepareStatement(insertSQL);

			pstmt.setString(1,id);

			pstmt.executeUpdate();

		}catch(SQLException e) {
			e.printStackTrace();

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}
}
