package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDAO;

/**
 * Servlet implementation class Regist
 */
@WebServlet("/Regist")
public class Regist extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Regist() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/Regist.jsp");
		dispatcher.forward(request, response);

		HttpSession session=request.getSession(false);
		if(session==null) {
			session=request.getSession(true);
			response.sendRedirect("/WEB-INF/jsp/Login.jsp");
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		Connection conn=null;
		String loginID=request.getParameter("loginId");
		String password=request.getParameter("password1");
		String password2=request.getParameter("password2");
		String name=request.getParameter("name");
		String address=request.getParameter("address");
		String userName=request.getParameter("userName");
		String phoneNumber=request.getParameter("phoneNumber");
		String birthDate=request.getParameter("birth");
		UserDAO userDao=new UserDAO();
		try {
			if(loginID.isEmpty()|| password.isEmpty()||password2.isEmpty()||name.isEmpty()||address.isEmpty()
					||userName.isEmpty()||phoneNumber.isEmpty()||birthDate.isEmpty()) {
				request.setAttribute("errMsg", "空欄があります。");

				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/Regist.jsp");
				dispatcher.forward(request, response);
				return;

			}else if(!(password.equals(password2))){
				request.setAttribute("errMsg", "パスワードが一致しません。");

				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/Regist.jsp");
				dispatcher.forward(request, response);
				return;

			}else if(userDao.findById(loginID)!=null){
				request.setAttribute("errMsg", "既にあるIDです。");

				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/Regist.jsp");
				dispatcher.forward(request, response);
				return;

			}

			String result=userDao.Code(password);
			userDao.userRegist(loginID,result,name,address,userName,phoneNumber,birthDate);
			response.sendRedirect("Login");

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
