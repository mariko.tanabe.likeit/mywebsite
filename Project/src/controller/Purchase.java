package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDAO;


/**
 * Servlet implementation class Purchase
 */
@WebServlet("/Purchase")
public class Purchase extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// セッション
		HttpSession session=request.getSession(false);
		try {

			if(session==null) {
				session=request.getSession(true);
				response.sendRedirect("/WEB-INF/jsp/Login.jsp");
			}
			ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

			if (cart.size() == 0) {
				request.setAttribute("cartActionMessage", "購入する商品がありません");
				response.sendRedirect("Cart");
			}


			//買い物かご
			ArrayList<ItemDataBeans> cartIDBList = (ArrayList<ItemDataBeans>) session.getAttribute("cart");
			//合計金額
			int totalPrice =ItemDAO.getTotalItemPrice(cartIDBList);


			BuyDataBeans bdb = new BuyDataBeans();
			UserDataBeans user=(UserDataBeans) session.getAttribute("userInfo");
			bdb.setUserId(user.getId());
			bdb.setTotalPrice(totalPrice);

			//購入確定で利用
			session.setAttribute("bdb", bdb);
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/Purchase.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
}
