package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ItemDAO;

/**
 * Servlet implementation class GoodsAdd
 */
@WebServlet("/GoodsAdd")
public class GoodsAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GoodsAdd() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/AdminGoodsAdd.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		Connection conn=null;
		String image=request.getParameter("datafile");
		String name=request.getParameter("itemName");
		String detail=request.getParameter("detail");
		String price=request.getParameter("price");
		ItemDAO iDao=new ItemDAO();
		try {
			if(image.isEmpty()|| name.isEmpty()||detail.isEmpty()||price.isEmpty()) {
				request.setAttribute("errMsg", "空欄があります。");

				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/AdminGoodsAdd.jsp");
				dispatcher.forward(request, response);
				return;

			}


			iDao.itemRegist(image,name,detail,price);
			response.sendRedirect("GoodsList");

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
