package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class AdminMypage
 */
@WebServlet("/AdminMypage")
public class AdminMypage extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminMypage() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session=request.getSession(false);
		if(session==null) {
			session=request.getSession(true);
			response.sendRedirect("/WEB-INF/jsp/Login.jsp");
		}


		UserDAO userDao=new UserDAO();
		List<UserDataBeans> userList=userDao.findAll();

		request.setAttribute("userList", userList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/AdminMypage.jsp");
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginId=request.getParameter("login-id");
		String name=request.getParameter("user-name");
		String birthData=request.getParameter("date");
		UserDAO userDao=new UserDAO();

		List<UserDataBeans> serchList=userDao.serch(loginId, name, birthData);

		request.setAttribute("userList", serchList);
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/AdminMypage.jsp");
		dispatcher.forward(request, response);


	}

}
