package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.ContactsDAO;
import dao.UserDAO;

/**
 * Servlet implementation class Contacts
 */
@WebServlet("/Contacts")
public class Contacts extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Contacts() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession(false);
		if(session==null) {
			session=request.getSession(true);
			response.sendRedirect("/WEB-INF/jsp/Login.jsp");
		}

		String id=request.getParameter("id");
		System.out.println(id);
		UserDAO userDao=new UserDAO();
		UserDataBeans user=userDao.userDetail(id);
		request.setAttribute("user", user);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/Contact.jsp");
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		Connection conn=null;
		String id=request.getParameter("id");
		System.out.println(id);

		String address=request.getParameter("mailaddress");
		String detail=request.getParameter("detail");

		ContactsDAO cDao=new ContactsDAO();
		try {
			if(address.isEmpty()||detail.isEmpty()) {
				request.setAttribute("errMsg", "空欄があります。");

				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/Contacts.jsp");
				dispatcher.forward(request, response);
				return;
			}
			cDao.contactRegist(id,address,detail);
			UserDAO userDao=new UserDAO();
			UserDataBeans user=userDao.userDetail(id);
			request.setAttribute("user", user);
			response.sendRedirect("MyPage");

		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
