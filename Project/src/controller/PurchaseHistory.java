package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.UserDataBeans;
import dao.BuyDAO;

/**
 * Servlet implementation class PurchaseHistory
 */
@WebServlet("/PurchaseHistory")
public class PurchaseHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PurchaseHistory() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {


			BuyDataBeans bdb = new BuyDataBeans();
			UserDataBeans user=(UserDataBeans) session.getAttribute("userInfo");
			BuyDAO bdd=new BuyDAO();
			List<BuyDataBeans> buylist=bdd.getBuyId(user.getId());
			request.setAttribute("buylist", buylist);


			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/Purchasehistory.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
