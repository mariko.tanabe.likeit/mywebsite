package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class GoodsDelete
 */
@WebServlet("/GoodsDelete")
public class GoodsDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GoodsDelete() {
        super();

    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id=request.getParameter("id");
		System.out.println(id);
		ItemDAO itemDao=new ItemDAO();
		ItemDataBeans item=itemDao.itemDetail(id);
		request.setAttribute("item", item);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/AdminGoodsDelete.jsp");
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String id=request.getParameter("id");
		System.out.println(id);

		ItemDAO itemDao=new ItemDAO();
		itemDao.itemDelete(id);

		response.sendRedirect("GoodsList");
	}

}
