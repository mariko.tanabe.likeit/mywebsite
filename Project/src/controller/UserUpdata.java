package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;


/**
 * Servlet implementation class UserUpdata
 */
@WebServlet("/UserUpdata")
public class UserUpdata extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdata() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession(false);
		if(session==null) {
			session=request.getSession(true);
			response.sendRedirect("/WEB-INF/jsp/Login.jsp");
		}

		String id=request.getParameter("id");
		System.out.println(id);
		UserDAO userDao=new UserDAO();
		UserDataBeans user=userDao.userDetail(id);
		request.setAttribute("user", user);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/Userupdata.jsp");
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		Connection conn=null;

		String id=request.getParameter("id");

		String password=request.getParameter("password1");
		String password2=request.getParameter("password2");
		String address=request.getParameter("address");
		String name=request.getParameter("userName");
		String phoneNumber=request.getParameter("phoneNumber");
		UserDAO userDao=new UserDAO();

		try {
			if(name.isEmpty()||address.isEmpty()||phoneNumber.isEmpty()) {
				request.setAttribute("errMsg", "空欄があります");

				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/Userupdata.jsp");
				dispatcher.forward(request, response);
				return;

			}else if(!(password.equals(password2))){
				request.setAttribute("errMsg", "パスワードが一致しません");

				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/Userupdata.jsp");
				dispatcher.forward(request, response);
				return;

			}
			if(password.isEmpty()&& password2.isEmpty()) {
				userDao.userUpdata2(address,name,phoneNumber,id);
				UserDataBeans user=userDao.userDetail(id);

				HttpSession session=request.getSession();
				session.setAttribute("user", user);
				response.sendRedirect("MyPage");
			}else if(password.isEmpty()|| password2.isEmpty()){
				request.setAttribute("errMsg", "空欄があります。");

				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/Userupdata.jsp");
				dispatcher.forward(request, response);
				return;
			}else {
				String result=userDao.Code(password);
				userDao.userUpdata(result,address,name,phoneNumber,id);
				UserDataBeans user=userDao.userDetail(id);

				HttpSession session=request.getSession();
				session.setAttribute("user", user);
				response.sendRedirect("MyPage");
			}
		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
