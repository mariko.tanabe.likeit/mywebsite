package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ContactDataBeans;
import dao.ContactsDAO;

/**
 * Servlet implementation class ListOfRequests
 */
@WebServlet("/ListOfRequests")
public class ListOfRequests extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListOfRequests() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession(false);
		if(session==null) {
			session= request.getSession(true);
			response.sendRedirect("/WEB-INF/jsp/Login.jsp");
		}

		try {
			ArrayList<ContactDataBeans> List=ContactsDAO.getAllData();
			request.setAttribute("List", List);

			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/ListOfRequests.jsp");
			dispatcher.forward(request, response);
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String id=request.getParameter("id");
		System.out.println(id);

		ContactsDAO itemDao=new ContactsDAO();
		itemDao.contactDelete(id);

		System.out.println("削除完了");

		response.sendRedirect("ListOfRequests");
	}

}
