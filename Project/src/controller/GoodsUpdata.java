package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDAO;
import dao.UserDAO;

/**
 * Servlet implementation class GoodsUpdata
 */
@WebServlet("/GoodsUpdata")
public class GoodsUpdata extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GoodsUpdata() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession(false);
		if(session==null) {
			session=request.getSession(true);
			response.sendRedirect("/WEB-INF/jsp/Login.jsp");
		}

		String id=request.getParameter("id");
		System.out.println(id);

		ItemDAO iDao=new ItemDAO();
		ItemDataBeans item=iDao.itemDetail(id);

		request.setAttribute("item", item);
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/AdminGoodsUpdata.jsp");
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		Connection conn=null;

		String id=request.getParameter("id");

		String name=request.getParameter("itemName");
		String detail=request.getParameter("detail");
		String price=request.getParameter("price");

		ItemDAO itemDao=new ItemDAO();

		try {
			if(name.isEmpty()||detail.isEmpty()||price.isEmpty()){
				request.setAttribute("errMsg", "空欄があります");

				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/AdminGoodsUpdata.jsp");
				dispatcher.forward(request, response);
				return;

			}else {
				itemDao.GoodsUpdata(name,detail,price,id);


				ItemDataBeans item=itemDao.itemDetail(id);
				
				UserDAO userDao=new UserDAO();
				UserDataBeans user=userDao.userDetail("1");

				HttpSession session=request.getSession();
				session.setAttribute("user", user);
				request.setAttribute("item", item);
				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/GoodsDetail.jsp");
				dispatcher.forward(request, response);
			}
		}finally {
			if(conn !=null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
