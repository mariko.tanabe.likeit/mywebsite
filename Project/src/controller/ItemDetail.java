package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class ItemDetail
 */
@WebServlet("/ItemDetail")
public class ItemDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession(false);
		if(session==null) {
			session= request.getSession(true);
			response.sendRedirect("/WEB-INF/jsp/Login.jsp");
		}

		String id=request.getParameter("id");
		System.out.println(id);

		ItemDAO iDao=new ItemDAO();
		ItemDataBeans item=iDao.itemDetail(id);

		request.setAttribute("item", item);
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/GoodsDetail.jsp");
		dispatcher.forward(request, response);
	}

}
