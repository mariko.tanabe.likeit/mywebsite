package beans;

public class ItemDataBeans {
	private int id;
	private String itemName;
	private int itemPrice;
	private String detail;
	private String image;

	public ItemDataBeans() {

	}
	public ItemDataBeans(int id,String name,int price) {
		this.id=id;
		this.itemName=name;
		this.itemPrice=price;
	}

	public ItemDataBeans(int id,int price) {
		this.id=id;
		this.itemPrice=price;
	}

	public ItemDataBeans(int id, String name,int itemPrice,String detail, String image) {
		this.id=id;
		this.itemName=name;
		this.itemPrice=itemPrice;
		this.detail=detail;
		this.image=image;
	}

	public ItemDataBeans(ItemDataBeans item) {

	}

	public int getId() {
		return id;
	}

	public void setId(int itemId) {
		this.id = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(int itemPrice) {
		this.itemPrice = itemPrice;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getFormatPrice() {
		return String.format("%,d", this.itemPrice);
	}
}
