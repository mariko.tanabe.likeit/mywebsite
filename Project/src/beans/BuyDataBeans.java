package beans;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BuyDataBeans {
	private int id;
	private int userId;
	private int totalPrice;
	private Date buyDate;

	public BuyDataBeans() {

	}

	public BuyDataBeans(int id, int totalprice, Date buydate) {
		this.id=id;
		this.totalPrice=totalprice;
		this.buyDate=buydate;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id=id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId=userId;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice=totalPrice;
	}
	public String getFormatTotalPrice() {
		return String.format("%,d", this.totalPrice);
	}
	public Date getBuyDate() {
		return buyDate;
	}
	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}
	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(buyDate);
	}

}
