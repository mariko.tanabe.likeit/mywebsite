package beans;

import java.util.Date;

public class UserDataBeans {
	private int id;
	private String loginId;
	private String password;
	private String name;
	private String address;
	private String userName;
	private int phoneNumber;
	private Date birthDate;
	private String createDate;

	public UserDataBeans() {

	}

	public UserDataBeans(int id,String loginId,String name,Date birthDate) {
		this.id=id;
		this.loginId=loginId;
		this.name=name;
		this.birthDate=birthDate;
	}

	public UserDataBeans(int id,String loginId,String password,String name,String address,String userName,int phoneNumber,Date birthDate,String createDate){
		this.id=id;
		this.loginId=loginId;
		this.password=password;
		this.name=name;
		this.address=address;
		this.userName=userName;
		this.phoneNumber=phoneNumber;
		this.birthDate=birthDate;
		this.createDate=createDate;
	}

	public UserDataBeans(int id,String name) {
		this.id=id;
		this.name=name;
	}

	public UserDataBeans(String loginId) {
		this.loginId=loginId;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId=loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Date getbirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate=createDate;
	}
}
