CREATE DATABASE fanclub DEFAULT CHARACTER SET utf8;
USE fanclub;
CREATE TABLE t_item(id int PRIMARY KEY AUTO_INCREMENT, item_name varchar(100) NOT NULL, price int NOT NULL,detail varchar(200) NOT NULL,image varchar(256) NOT NULL);
CREATE TABLE t_user(id int PRIMARY KEY AUTO_INCREMENT, login_id varchar(30) UNIQUE NOT NULL, password varchar(50)NOT NULL,name varchar(50)NOT NULL,
address varchar(256)NOT NULL, user_name varchar(25)NOT NULL,phonenumber int NOT NULL,birth_date date NOT NULL,create_date date NOT NULL);

CREATE TABLE buy_data(id int PRIMARY KEY AUTO_INCREMENT,user_id int NOT NULL, total_price int NOT NULL, create_date date NOT NULL);
CREATE TABLE buy_detail(id int PRIMARY KEY AUTO_INCREMENT, buy_id int NOT NULL, item_id int NOT NULL);
CREATE TABLE contact_t(id int PRIMARY KEY AUTO_INCREMENT, user_id int NOT NULL, mailaddress varchar(100) NOT NULL, contents varchar(1200)NOT NULL);

ALTER TABLE t_user CHANGE COLUMN adress address varchar(256) NOT NULL;
ALTER TABLE contact_t CHANGE COLUMN mailadress mailaddress varchar(100) NOT NULL;

INSERT INTO t_user (login_id,name,birth_date,password,address,user_name,phonenumber,create_date) 
VALUES ('admin','管理者太郎','19900202','password','東京','管理者',056,now());

INSERT INTO t_item(item_name,price,detail,image) VALUES('ABC写真集',2500,'念願の南米・ウユニ塩湖に降り立ち、圧巻の景色の中佇む佐藤健の旅路を追った写真に加え、まるでドキュメントムービーのようなDVD映像も存分に収めた超豪華永久保存版のアニバーサリーブックが完成。'
,'https://2.bp.blogspot.com/-WnWg9ZWut3s/Wrx8ZiO5M8I/AAAAAAABLDU/7yVFwlXVsMobQXG-4SzUptAIjMxnyGTyQCLcBGAs/s800/book_idol_syashinsyu_man.png');

SELECT*FROM t_item;

SELECT item_id, price FROM buy_data INNER JOIN buy_detail ON buy_data.id=buy_detail.buy_id INNER JOIN t_item ON buy_detail.item_id=t_item.id WHERE buy_id = 1;
SELECT distinct buy_id, total_price,create_date FROM buy_detail INNER JOIN buy_data ON buy_detail.buy_id=buy_data.id WHERE user_id = 2;
